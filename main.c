#include <stddef.h>
#include <stdlib.h>
#include <locale.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#define _XOPEN_SOURCE_EXTENDED

#define BUFFER_SIZE 256
#define STATUS_OK 0
#define STATUS_COLLIDE_WALL 1
#define STATUS_COLLIDE_SELF 2

#include <curses.h>
typedef struct {
  int x;
  int y;
  int frame;
} cell_t;

typedef struct {
  int frame;
  int head;
  int tail;
  int length;
  int status;
  cell_t food;
  cell_t buffer[BUFFER_SIZE];
} state_t;
// Originally found here: https://stackoverflow.com/a/6852396
// I modified it to make max exclusive
// Assumes 0 <= max <= RAND_MAX
// Returns in the half-open interval [0, max[
long random_at_most(long max) {
  unsigned long
    // max <= RAND_MAX < ULONG_MAX, so this is okay.
    num_bins = (unsigned long) max,
    num_rand = (unsigned long) RAND_MAX + 1,
    bin_size = num_rand / num_bins,
    defect   = num_rand % num_bins;

  long x;
  do {
   x = random();
  }
  // This is carefully written not to overflow
  while (num_rand - defect <= (unsigned long)x);

  // Truncated division is intentional
  return x/bin_size;
}

void randomize(cell_t * cell){
  cell->x=1+random_at_most(COLS-2);
  cell->y=1+random_at_most(LINES-2);
}

cchar_t border_tl = {0};
cchar_t border_t = {0};
cchar_t border_tr = {0};
cchar_t border_l = {0};
cchar_t border_r = {0};
cchar_t border_bl = {0};
cchar_t border_b = {0};
cchar_t border_br = {0};
cchar_t snake_tail[4] = {{0},{0},{0},{0}};
cchar_t snake_head = {0};
cchar_t food = {0};

void init_state(state_t * s) {
  s->head=0;
  s->tail=0;
  s->frame=0;
  s->length=25;
  s->status=STATUS_OK;
  randomize(&s->food);
  s->food.frame=0;
  for(int i=0;i<BUFFER_SIZE;i++){
    s->buffer[i].x=-1;
    s->buffer[i].y=-1;
    s->buffer[i].frame=-1;
  }
  s->buffer[0].x=COLS/2;
  s->buffer[0].y=LINES/2;
}
void step(state_t * s, cell_t * v) {
  int nextHead = (s->head + 1) % BUFFER_SIZE;
  cell_t * curCell = &(s->buffer[s->head]);
  cell_t * nextCell = &(s->buffer[nextHead]);
  int dist = (BUFFER_SIZE + s->head - s->tail) % BUFFER_SIZE;
  if(dist >= s->length){
    s->tail = (s->tail + 1) % BUFFER_SIZE;
  }
  nextCell->x = curCell->x + v->x;
  nextCell->y = curCell->y + v->y;
  for(int i=s->tail;i != s->head; i=(i+1)%BUFFER_SIZE) {
    if(nextCell->x==s->buffer[i].x && nextCell->y==s->buffer[i].y){
      s->status = s->status | STATUS_COLLIDE_SELF;
    }
  }
  if(nextCell->x < 1
      || nextCell->x > COLS - 2
      || nextCell->y < 1
      || nextCell->y > LINES - 2
    ) {
    s->status = s->status | STATUS_COLLIDE_WALL;
  }
  if(nextCell->x == s->food.x && nextCell->y == s->food.y) {
    s->length += 5;
    randomize(&s->food);
    s->food.frame=s->frame;
  }
  s->frame++;
  if(s->status == STATUS_OK) {
    nextCell->frame=s->frame;
    s->head = nextHead;
  }
}

void draw(state_t * s){
  cell_t * c = &s->buffer[s->head];
  mvadd_wch(c->y, c->x, &snake_head);
  mvadd_wch(s->food.y, s->food.x, &food);
  for(int i=0;i<4;i++) {
    c = &s->buffer[(s->tail+i) % BUFFER_SIZE];
    if(c->frame<0){
      break;
    }
    mvadd_wch(c->y, c->x, &snake_tail[i]);
  }
  if(s->status & STATUS_COLLIDE_WALL) {
    mvaddstr(LINES/2,COLS/2, "You hit a wall!");
  }
  if(s->status & STATUS_COLLIDE_SELF) {
    mvaddstr(LINES/2+1,COLS/2, "You hit yourself, dummie!");
  }
}


void init_chars() {
  bool hasErr = false;


  hasErr = hasErr || setcchar(&border_tl, L"╔", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&border_t,  L"═", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&border_tr, L"╗", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&border_l,  L"║", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&border_r,  L"║", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&border_bl, L"╚", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&border_b,  L"═", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&border_br, L"╝", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&snake_head, L"█", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&snake_tail[3], L"▓", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&snake_tail[2], L"▒", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&snake_tail[1], L"░", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&snake_tail[0], L" ", WA_NORMAL, 1, NULL);
  hasErr = hasErr || setcchar(&food, L"★", WA_NORMAL, 1, NULL);

  if (hasErr) {
    printf("error initializing border characters");
    exit(-1);
  }
};

cell_t vLeft = {-1,0};
cell_t vRight = {1,0};
cell_t vUp = {0,-1};
cell_t vDown = {0,1};

cell_t * reset(state_t * s) {
  init_state(s);
  clear();
  border_set(&border_l, &border_r, &border_t, &border_b,
      &border_tl, &border_tr, &border_bl, &border_br);
  switch(random_at_most(4)){
    case 0: return &vLeft;
    case 1: return &vRight;
    case 2: return &vUp;
    case 3: return &vDown;
  }
  exit(-1);
}

int main(void)
{
  char * locale = setlocale(LC_ALL, "");
  printf("using locale %s\n", locale);
  WINDOW * win = initscr();
  cbreak();
  nodelay(win,true);
  noecho();
  curs_set(0);
  if(OK!=start_color()){
    printf("doof %d\n",start_color());
    exit(-1);
  }
  init_pair(1, COLOR_RED, COLOR_BLACK);

  init_chars();

  char c=' ';
  touchwin(win);
  wrefresh(win);
  refresh();

  state_t state;
  cell_t * v = reset(&state);
  while(c!='q'){
    usleep(100000);
    mvprintw(LINES-2,2,"frame: %04d, head: %03d, tail: %03d, length: %03d",state.frame, state.head, state.tail, state.length);
    refresh();
    c=getch();
    switch(c) {
      case 'a':
        v = &vLeft;
        break;
      case 's':
        v = &vDown;
        break;
      case 'd':
        v = &vRight;
        break;
      case 'w':
        v = &vUp;
        break;
      case 'r':
        v=reset(&state);
    }
    step(&state, v);
    draw(&state);
  }
  endwin();
  return 0;
}

