# cursed snake

One of those games that are literally simple enough to code in an afternoon.
Yet, I remembered that as a kid, learning Basic
on my father's Alphatronic P2, I wanted to do something similar but I was
only starting back then and I had absolutely no clue how to do it.

Watching [this guy on youtube] doing it on an Apple ][ made me want to try it
again, roughly 40 years later.

Using an emulator for some obscure 80s hardware would be fun, but for
starters I settled with plain old C and curses for dealing with the
terminal stuff. It's not the same, I know.

